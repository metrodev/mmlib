#!/usr/bin/env python
import argparse
import os

import balance

SECRETS_FILENAME = '.secrets'

parser = argparse.ArgumentParser(description='Obtener el balance actual de movistar')

parser.add_argument('-g', '--guardar', help='Guardar el usuario y contraseña para futuro uso', action='store_true')
parser.add_argument('-n', '--numero', metavar='0414/0424-1111111', type=str, help='Numero de telefono')
parser.add_argument('-c', '--contra', metavar='hunter12', type=str, help='Contraseña de mimovistar')

if __name__ == "__main__":
  args = parser.parse_args()
  
  username = args.numero
  password = args.contra

  if os.path.exists(SECRETS_FILENAME):
    print("Usando usuario y contraseña guardados")

    with open(SECRETS_FILENAME, 'r') as file:
      username, password = file.readlines()

  if not username:
    print("Debes ingresar un numero!")
  if not password:
    print("Debes ingresar una contraseña!")
  if not (username and password):
    print("\n")
    parser.print_help()
    exit()

  print("Intentando entrar en mi movistar")

  if balance.login(username, password):
    print("Entramos :)")
    if args.guardar:
      print("Guardando datos...")
      with open(SECRETS_FILENAME, 'w+') as file:
        file.writelines([username, password])

    print("Intentando parsear la respuesta")
    try:
      balance = balance.get_balance()
      print(f"Saldo: {balance}")
    except Exception as e:
      print(f"Algo salio mal :( {e}")
  else:
    print("Usuario y contraseña incorrectos :(")



