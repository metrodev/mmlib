Mi Movistar
====
![en funcion](doc/screen2.jpg)

Libreria (Aunque tambien funciona como ejecutable) para leer el saldo de una cuenta de mi movistar de manera automatizada

Nota: solo funciona si tienes los datos de tu cuenta (usuario y contraseña) :)

## Instalacion
```bash
git clone https://gitlab.com/metrodev/mmlib.git
pip install -r requirements.txt
```

## Uso
![en funcion](doc/screen1.jpg)
```
./saldo.py -n NUMERO -c CONTRA
```

`-n` es el numero de Telefono
`-c` es la contraseña para entrar a mi movistar

Licencia
===
MIT