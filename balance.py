"""
Get current account's balance
"""
import requests
from bs4 import BeautifulSoup

sess = requests.Session()

"""
Login en movistar

retorna True si fue exitoso, False si no
"""
def login(username, password):
  form_data = {
    'AJAXREQUEST': 'panel',
    'login': 'login', # I have no idea why
    'login:usuarioParticular': username,
    'login:clave': password,
    'login:claveTxt': '',
    'login:clave1': password,
    'javax.faces.ViewState': 'j_id1', # no idea
    'login:ingresar': 'login:ingresar', # no idea again
    '':''
  }

  """
  So, for some reason they expect you to actually navigate to the page
  so that they can set a cookie for you AND THEN you may log in
  otherwise you'll get the boot

  let's go and get that lousy cookie
  disregard response
  """
  sess.get('https://mi.movistar.com.ve/index.jsf')

  response = sess.post('https://mi.movistar.com.ve/index.jsf', data=form_data, allow_redirects=False, headers={'sec-ch-ua': 'blurry/.0'})
  return response.headers['Ajax-Response'] == 'redirect'

"""
Obtiene el saldo actual de la cuenta,

retorna tipo float
"""
def get_balance():
  response = sess.get('https://mi.movistar.com.ve/dashboard/contenedorDashboard.jsf', headers={
    'Referer': 'https://mi.movistar.com.ve/index.jsf',
    'sec-ch-ua': 'blurry/.0'
  })

  d = BeautifulSoup(response.text, 'html5lib')
  balance = d.find(class_='montoSaldoDeuda').get_text()

  balance = balance.strip().replace('.', '').replace(',', '.')

  return float(balance)

"""
alias de get_balance
"""
def get_saldo():
  return get_balance()